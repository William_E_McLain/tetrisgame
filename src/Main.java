import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JFrame
        implements ActionListener, MouseListener, KeyListener {

    private GamePanel gamePanel;
    //private final GameData gameData;
    private Animator animator;
    private JButton startButton;
    private JButton quitButton;
    //private Launcher launcher;
    private    JButton playButton = new JButton("PLAY GAME");
    private    JButton optionsButton = new JButton("OPTIONS");
    private    JButton highScoresButton = new JButton("HIGH SCORES");  
    public Main() {
        setSize(500, 600);
        setLocation(100, 100);
        setResizable(false);
//        Container c = getContentPane();
//        animator = new Animator();
//        gameData = new GameData();
//        gamePanel = new GamePanel(animator, gameData);
//        animator.setGamePanel(gamePanel);
//        animator.setGameData(gameData);
//        c.add(gamePanel, "Center");
//
//        JPanel southPanel = new JPanel();
//        startButton = new JButton("Start");
//
//        southPanel.add(startButton);
//        quitButton = new JButton("Quit");
//        southPanel.add(quitButton);
//        c.add(southPanel, "South");

       // OptionsPanel oPanel = new OptionsPanel();
        Container c = getContentPane();

        
        playButton.addActionListener(this);
        optionsButton.addActionListener(this);
        highScoresButton.addActionListener(this);

        JPanel southPanel = new JPanel();

        c.add(southPanel, "South");

        southPanel.add(playButton, "South");
        southPanel.add(optionsButton, "Center");
        southPanel.add(highScoresButton, "Center");

        
     //   gamePanel.addMouseListener(this);
    //    startButton.setFocusable(false); // "Start" button click should not receive keyboard data
     //   gamePanel.setFocusable(true); // receives keyboard data
     //   gamePanel.addKeyListener(this);
       
        
//        playButton.addActionListener(this);
//        optionsButton.addActionListener(this);
//        highScoresButton.addActionListener(this);

    //    launcher = (Launcher) gameData.figures.get(0); // launcher
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == playButton) {
           System.out.println("Play Game");
            // gamePanel.startGame();
        } else if (ae.getSource() == quitButton) {
            animator.running = false;  
        }
        else if (ae.getSource() == optionsButton){
             System.out.println("Options");
               this.setVisible(false);
             new OptionsJFrame();   
               this.setVisible(false);
        }
        else if (ae.getSource() == highScoresButton){
             System.out.println("High Scores");
             this.setVisible(false);
             new HighScoresJFrame();
             this.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        Color color;
        double r = Math.random();
        if (r < 0.25) {
            color = Color.red;
        } else if (r < 0.5) {
            color = Color.blue;
        } else if (r < 0.75) {
            color = Color.gray;
        } else {
            color = Color.green;
        }

       // Missile f = new Missile(launcher.getXofMissileShoot(), launcher.getYofMissileShoot(), color);
        f.setTarget(x, y);
        int size = (int) (Math.random() * 100) + 5; // min = 5 max = 105
        f.setExplosionMaxSize(size);

        synchronized (gameData.figures) {
            gameData.figures.add(f);
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_LEFT:
               // launcher.x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
              //  launcher.x += 10;
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    public static void main(String[] args) {
       
        System.out.println("Program Start");
        JFrame game = new Main();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
    }
}
