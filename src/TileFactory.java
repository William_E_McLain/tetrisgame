
import java.awt.Image;


// participant: FlyweightFactory

public class TileFactory {

     // Pool for one tile only
     // if there are more tile types this can be an
     // array or list or better a HashMap
     
    private static Tile tile;

    public static Tile getTile(Image tileImage) {

        // this is a singleton 
        // if there is no tile 
        if (tile == null) {
            tile = new TileConcrete(tileImage);
        }

        return tile;
    }
}
