// participant: Flyweight
//      declares an interface through which flyweights can receive
//      and act on extrinsic state.
// source: www.oodesign.com

public interface Tile {

    
     // Move Soldier From Old Location to New Location.
     // Note that tile location
     //          is extrinsic to the SoldierFlyweight Implementation
    public void moveTile(int previousLocationX,
            int previousLocationY, int newLocationX, int newLocationY);
}
