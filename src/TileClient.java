// participant: Client

 // This is the "Heavyweight" tile object which is the client of the flyweight
 // tile this object provides all tile services and is used in the game
 
public class TileClient {

    // Reference to the flyweight
    private Tile tile = TileFactory.getTile(tileImage);
    
    // this state is maintained by the client
    private int currentLocationX = 0;

    // this state is maintained by the client
    private int currentLocationY = 0;

    public void moveTile(int newLocationX, int newLocationY) {

        // here the actual rendering is handled by the flyweight object 
        tile.moveTile(currentLocationX,
                currentLocationY, newLocationX, newLocationY);

        // this object is responsible for maintaining the state
        // that is extrinsic to the flyweight
        currentLocationX = newLocationX;

        currentLocationY = newLocationY;
    }
}
