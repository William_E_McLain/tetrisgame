import java.awt.Graphics;

public interface GameFigure {
    public void render(Graphics g);
    public void update();
    public int getState();
    static final int STATE_FALLING = 1;
    static final int STATE_ROTATING = 2;
    static final int STATE_DONE = 0;
}
