import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;

public class OptionsPanel extends JPanel{
    public static final int PWIDTH = 500;
    public static final int PHEIGHT = 600;
	
    
    // off screen rendering
    private Graphics graphics;
    private Image dbImage = null;
    
    public OptionsPanel ()
    {
	
	setBackground(Color.yellow);
	setPreferredSize(new Dimension(PWIDTH, PHEIGHT));
    }
}
