
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

class OptionsJFrame extends JFrame {

        public OptionsJFrame() {
           // removeAll();

            // setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(500, 600);
            setLocation(30, 21);
            JLabel optionsLabel = new JLabel("Options");
            Container c = getContentPane();
            JPanel oSouthPanel = new JPanel();
            JPanel oNorthPanel = new JPanel();
            JPanel oCenterPanel= new JPanel();
//            c.add(oSouthPanel,"South");
//            c.add(oSouthPanel,"North");
            final JRadioButton soundOptions = new JRadioButton("Sound",false);
            final JButton returnToMenuButton = new JButton("Return to Menu");
            oNorthPanel.add(optionsLabel, "North");
            oSouthPanel.add(returnToMenuButton, "South");
            oCenterPanel.add(soundOptions,"Center");
            c.add(oCenterPanel,"Center");
            c.add(oSouthPanel, "South");
            c.add(oNorthPanel, "North");
            revalidate();
            repaint();
            setVisible(true);
            returnToMenuButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == returnToMenuButton) {
                        System.out.println("Close Options Menu");
                        dispose();
                    }
                }

            });

//pack();
        }
    }