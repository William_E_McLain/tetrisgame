
import java.awt.Graphics;


public class TetrominoFactory implements GameFigure{
    
  final private int state = STATE_FALLING;
    
    public TetrominoFactory getPeice(String tetrominoName) {
        switch (tetrominoName) {
            case "I_TETROMINO": {
                return new ITetromino();    // returns a new instance of ITetromino();
            }

            case "J_TETROMINO": {
                return new JTetromino();   // returns a new instance of JTetromino();
            }
            case "L_TETROMINO": {
                return new LTetromino();// returns a new instance of LTetromino();
            }
            case "O_TETROMINO": {
                return new OTetromino();// returns a new instance of OTetromino();
            }
            case "S_TETROMINO": {
                return new STetromino();// returns a new instance of STetromino();
            }
            case "T_TETROMINO": {
                return new TTetromino();// returns a new instance of TTetromino();
            }
            case "Z_TETROMINO": {
                return new ZTetromino();// returns a new instance of ZTetromino();
            }

            default: {
                throw new IllegalArgumentException(tetrominoName + "Is not a recongnized game name");
            }
        }
    }

    public void move() {
    }

    
   

    
    public void setState() {
    }
    public void getLocation() {
    }
   
    public void setLocation(){
    }

    @Override
    public void render(Graphics g)
    {}
    @Override
    public void update()
    {
    }  
    @Override
     public int getState() {
         return this.state;
    }
}
