
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


    class HighScoresJFrame extends JFrame {

        public HighScoresJFrame() {

            //  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(500, 600);
            setLocation(30, 21);

          // removeAll();
            ArrayList<String> highScoreList = new ArrayList<>();
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");
            highScoreList.add("The Dude     5000000000");

            Container c = getContentPane();
            JPanel oSouthPanel = new JPanel();
            JPanel oNorthPanel = new JPanel();
            JPanel oCenterPanel = new JPanel();
            c.setBackground(Color.red);

            String highScores = "";
            for (int i = 0; i < highScoreList.size(); i++) {
                highScores = highScores + highScoreList.get(i) + '\n';
            }
            System.out.println(highScores);
            final JLabel scoresLabel = new JLabel("High Scores");
            final JTextArea scoreArea = new JTextArea(highScores);
            oCenterPanel.add(scoreArea, "Center");

            c.add(oCenterPanel, "Center");

            final JButton returnToMenuButton = new JButton("Return to Menu");

            oSouthPanel.add(scoresLabel, "North");
            c.add(oSouthPanel, "South");

            setVisible(true);
            oNorthPanel.add(scoresLabel, "North");
            oSouthPanel.add(returnToMenuButton, "South");

            c.add(oSouthPanel, "South");
            c.add(oNorthPanel, "North");

            returnToMenuButton.addActionListener(new ActionListener() {

                               @Override
                public void actionPerformed(ActionEvent ae) {
                    if (ae.getSource() == returnToMenuButton) {
                        System.out.println("Close High Score Menu");
                        dispose();
                    }
                }

                

            });
            //pack();
            revalidate();
            repaint();
            setVisible(true);

        }
    }