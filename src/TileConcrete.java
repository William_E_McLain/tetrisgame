
import java.awt.Image;

// participant: ConcreteFlyweight
public class TileConcrete implements Tile {

  
    // Intrinsic state maintained by flyweight implementation
    // Tile Shape (graphical represetation)
    //      how to display the tile is up to the flyweight implementation
     private Image tileImage;

   public TileConcrete(Image tileImage){
       
        this.tileImage = tileImage;
   }  
    @Override
    public void moveTile(int previousLocationX, int previousLocationY,
            int newLocationX, int newLocationY) {
        // delete tile representation from previous location 
        // then render tile representation in new location 	
    }
     
   
}
